
def make_key(prefix, id):
    """
    Function to be used by the models to create a custom key.
    :param prefix: The prefix to be used depending the model that called it.
    :param id: The id of the model instance that called it.
    :return: The key to be used by the model object in hexadecimal
    padded with zeros to be a 5 digit hexadecimal number.
    """
    return str(prefix + "{:0=5X}".format(id))