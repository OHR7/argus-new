from django.db import models

# Local app imports
from doctors.models import Doctor

# Create your models here.


class Patient(models.Model):
    prefix = 'PT'

    def make_key(self):
        return str(self.prefix + "{:0=5X}".format(self.id))

    address = models.CharField(max_length=50)
    birth_date = models.DateField()
    cell_phone = models.IntegerField()
    city = models.CharField(max_length=50)
    country = models.CharField(max_length=50, null=True)
    doctor = models.ForeignKey(Doctor, blank=True, null=True, on_delete=models.CASCADE)
    email = models.EmailField(max_length=50, blank=True, null=True)
    first_name = models.CharField(max_length=50, blank=True, null=True)
    first_last_name = models.CharField(max_length=50, blank=True, null=True)
    key = models.CharField(max_length=20, default=make_key)
    pc = models.IntegerField()
    phone = models.IntegerField()
    registration_date = models.DateTimeField(editable=True, auto_now=True, auto_now_add=False)
    second_last_name = models.CharField(max_length=50, blank=True, null=True)
    ssn = models.IntegerField()
    state = models.CharField(max_length=50, null=True)
    status = models.CharField(max_length=20, blank=True, null=True)

    # Alert = models.IntegerField()

    def __str__(self):
        return self.key
