from django.db import models

# Local app imports
from pages.models import User

# Create your models here.


class Client(models.Model):
    prefix = 'CA'

    def make_key(self):
        return str(self.prefix + "{:0=5X}".format(self.id))

    company_name = models.CharField(max_length=50)
    fact_email = models.EmailField(max_length=50)
    key = models.CharField(max_length=20, default=make_key)
    pc = models.IntegerField()
    rfc = models.CharField(max_length=50)
    social_reason = models.CharField(max_length=50)
    status = models.BooleanField(null=True, default=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.key
