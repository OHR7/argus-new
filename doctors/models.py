from django.db import models

# Local app imports
from clients.models import Client
from pages.models import User

# Create your models here.


class Doctor(models.Model):
    prefix = 'MD'

    def make_key(self):
        return str(self.prefix + "{:0=5X}".format(self.id))

    alert = models.IntegerField(default=0)
    client = models.ForeignKey(Client, blank=True, null=True, on_delete=models.CASCADE)

    # TODO change to property
    key = models.CharField(max_length=20, default=make_key)
    kits = models.IntegerField(default=0)
    patients = models.IntegerField(default=0)
    pc = models.IntegerField()
    position = models.CharField(max_length=50)
    rfc = models.CharField(max_length=50)
    specialty = models.CharField(max_length=50)
    status = models.BooleanField(null=True, default=True)
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    writ = models.IntegerField()

    def __str__(self):
        return self.user.key
