from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class User(AbstractUser):
    address = models.CharField(max_length=100)
    cell_phone = models.CharField(max_length=20)
    city = models.CharField(max_length=20)
    country = models.CharField(max_length=50, default='mx')
    key = models.CharField(max_length=20, unique=True)
    personal_email = models.EmailField(max_length=30)
    phone = models.CharField(max_length=20, blank=True, null=True)
    registration_date = models.DateTimeField(auto_now_add=True)
    second_last_name = models.CharField(max_length=20)
    state = models.CharField(max_length=20)

