from django.db import models

# Local app imports
from pages.models import User


# Create your models here.


class Administrator(models.Model):
    prefix = 'AA'

    def make_key(self):
        return str(self.prefix + "{:0=5X}".format(self.id))

    key = models.CharField(max_length=20, default=make_key)
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.key
