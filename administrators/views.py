from django.views.generic import ListView

# Local app imports
from clients.models import Client

# Create your views here.


class AdminHome(ListView):
    template_name = 'administrators/home.html'
    queryset = Client.objects.all()
